unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.ScrollBox, FMX.Memo,
  System.StrUtils, System.Rtti, FMX.Grid.Style, FMX.Grid, System.Zip,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef,
  FireDAC.Stan.ExprFuncs, FireDAC.FMXUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FMX.Edit;

type
  TForm1 = class(TForm)
    Button1: TButton;
    ProcessMemo: TMemo;
    StringGrid1: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    FDConnectionLite: TFDConnection;
    FDQInsert: TFDQuery;
    FDQclean: TFDQuery;
    Label1: TLabel;
    EditFiveChar: TEdit;
    Label2: TLabel;
    EditDuplicate: TEdit;
    Label3: TLabel;
    EditFirstLast: TEdit;
    CompairSizeGrid: TStringGrid;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
    DefaultDirectory: String;
    procedure StartTest;
    procedure ReportSizeDirectories;
    procedure GetSubInDirectories(dir: string);
    procedure CountMoreThanFive;
    procedure CountCharFirstLast;
    procedure CountCharSameMoreThanTwo;
    procedure UpdateFirstChar;
    procedure GeneratePdfFromDb;
    procedure CleanDb;
    procedure ZipDirectory;
    procedure CompairSize;
    function InsertData(Word: string): Boolean;
    function GetDirSize(dir: string; subdir: Boolean): Longint;
    function FileSize(fileName: wideString): Longint;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses synPDF, System.IOUtils;

constructor TForm1.Create(AOwner: TComponent);
begin
  Inherited;
  DefaultDirectory := GetCurrentDir+'\temp';
  if not DirectoryExists(DefaultDirectory) then
    CreateDir(DefaultDirectory);
end;

procedure TForm1.CleanDb;
var
  QDelete: TFDQuery;
begin
  // delete old data in database
  QDelete := TFDQuery.Create(nil);
  QDelete.Connection := FDConnectionLite;
  QDelete.SQL.Text := 'DELETE FROM dictionary';
  QDelete.ExecSQL;
  QDelete.Free;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Button1.Enabled:=False;
  CleanDb;
  ProcessMemo.Lines.Add('Start Process / '+TimeToStr(Now));
  StartTest;
end;

procedure TForm1.StartTest;
var
  DicFile, NewWordFile: TextFile;
  Word, RFolder, SFolder, FilePath: string;
  I: Integer;
  InsertResult: Boolean;
begin
  // Check file exists
  if FileExists('dictionary.txt') = FALSE then
  begin
    ShowMessage('File not found.');
    exit
  end;
  try
    // Assign file to variable
    AssignFile(DicFile, 'dictionary.txt');
    // Put pointer at the top
    Reset(DicFile);
    // Loop file
    while NOT eof(DicFile) do
    begin
      // read line in textfile
      Readln(DicFile, Word);
      if Word.Trim <> '' then
      begin
        // Get first char to root directory
        RFolder := copy(Word, 1, 1);
        FilePath := DefaultDirectory + '\' + RFolder.Trim;
        // check root directory if not exists
        if not DirectoryExists(FilePath) then
          CreateDir(FilePath);
        // check length word more than one char
        if length(Word) > 1 then
        begin
          // Get second char to sub directory
          SFolder := copy(Word, 2, 1);
          FilePath := FilePath + '\' + SFolder.Trim;
          // check sub directory if not exists
          if not DirectoryExists(FilePath) then
            CreateDir(FilePath);
        end;
        // create file with word
        AssignFile(NewWordFile, FilePath + '\' + Word + '.txt');
        // Rewrite creates a new external file with the name assigned
        ReWrite(NewWordFile);
        // write text in file 100 word
        Writeln(NewWordFile, DupeString(Word + ' ', 100));
        // close file
        InsertResult := InsertData(Word);
        CloseFile(NewWordFile);
      end;
    end;
    // Close file
    CloseFile(DicFile);
    ProcessMemo.Lines.Add('Create file complete. / '+TimeToStr(Now));
    if InsertResult = true then
    begin
      ProcessMemo.Lines.Add('Insert database complete. / '+TimeToStr(Now));
    end
    else
    begin
      ProcessMemo.Lines.Add('Insert database fail. / '+TimeToStr(Now));
    end;
  except
    on E: Exception do
    begin
      ProcessMemo.Lines.Add('Create file fail. / '+TimeToStr(Now));
    end;
  end;
  // Report directory size
  ReportSizeDirectories;
  // Zip direcroty
  ZipDirectory;
  // Compair size directory and zip file
  CompairSize;
  // Count more than fire char
  CountMoreThanFive;
  // Count char same more than two
  CountCharSameMoreThanTwo;
  // Count same char first last
  CountCharFirstLast;
  // Update first char upper case
  UpdateFirstChar;
  // Export PDF
  GeneratePdfFromDb;
  ProcessMemo.Lines.Add('process complete. / '+TimeToStr(Now));
  Button1.Enabled:=True;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  // RmDir(DefaultDirectory);
  TDirectory.Delete(DefaultDirectory, true);
end;

procedure TForm1.CompairSize;
var
  sr: TSearchRec;
  RowCount, found, FolderSize, ZipSize: Integer;
  Result: Float64;
begin
  try
    // Try to find regular in the current dir
    found := FindFirst(DefaultDirectory + '/*.*', faDirectory, sr);
    while found = 0 do
    begin
      // if directory and not . or ..
      if (sr.Attr = faDirectory) AND (sr.Name[1] <> '.') then
      begin
        Result := 0;
        RowCount := CompairSizeGrid.RowCount + 1;
        CompairSizeGrid.RowCount := RowCount;
        // Get Folder Size
        FolderSize := GetDirSize(DefaultDirectory + '/' + sr.Name, true);
        // Get Zip Size
        ZipSize := FileSize(DefaultDirectory + '/' + sr.Name + '.zip');
        // Calculate %
        Result := ((FolderSize - ZipSize) / FolderSize) * 100;
        CompairSizeGrid.Cells[0, RowCount - 1] := sr.Name;
        CompairSizeGrid.Cells[1, RowCount - 1] := IntToStr(FolderSize);
        CompairSizeGrid.Cells[2, RowCount - 1] := IntToStr(ZipSize);
        CompairSizeGrid.Cells[3, RowCount - 1] := FormatFloat('0.##', Result);
      end;
      found := FindNext(sr);
    end;
    // Free up resources
    FindClose(sr);
    ProcessMemo.Lines.Add('Compair size directories complete. / '+TimeToStr(Now));
  Except
    begin
      ProcessMemo.Lines.Add('Compair size directories fail. / '+TimeToStr(Now));
      exit
    end;
  end;
end;

function TForm1.FileSize(fileName: wideString): Longint;
var
  sr: TSearchRec;
begin
  if FindFirst(fileName, faAnyFile, sr) = 0 then
    Result := sr.Size
  else
    Result := -1;
  FindClose(sr);
end;

procedure TForm1.GeneratePdfFromDb;
var
  fileName: string;
  PDF: TPdfDocumentGDI;
  Page: TPdfPage;
  PosY: Integer;
  QSel: TFDQuery;
begin
  fileName := 'dictiionary.pdf';
  // create pdf
  PDF := TPdfDocumentGDI.Create();
  try
    // create new query
    QSel := TFDQuery.Create(nil);
    // assign connection
    QSel.Connection := FDConnectionLite;
    // close connection
    QSel.Active := FALSE;
    QSel.SQL.Text := 'SELECT word FROM dictionary';
    // active connection
    QSel.Active := true;
    // add info pdf
    PDF.Info.Title := 'dictiionary';
    PDF.Info.Author := 'Piyawat Damrongsuphakit';
    // add page
    Page := PDF.AddPage;
    // set page potrait
    Page.PageLandscape := FALSE;
    // set page size
    PDF.DefaultPaperSize := psA4;
    // set start position
    PosY := 10;
    if QSel.RecordCount > 0 then
    begin
      // move pointer first row
      QSel.First;
      while (not QSel.eof) do
      begin
        // set position y
        PosY := PosY + 20;
        // input word
        PDF.VCLCanvas.TextOut(50, PosY, QSel.FieldValues['word']);
        // check 50 record
        if QSel.RecNo mod 50 = 0 then
        begin
          // add page
          Page := PDF.AddPage;
          // set start position
          PosY := 10;
        end;
        QSel.Next;
      end;
    end;
    // create file
    PDF.SaveToFile(fileName);
    // Free resource
    PDF.Free;
    ProcessMemo.Lines.Add('Export PDF complete. / '+TimeToStr(Now));
  except
    ProcessMemo.Lines.Add('Export PDF fail. / '+TimeToStr(Now));
  end;
end;

procedure TForm1.CountCharFirstLast;
const
  Alphabet = ['a' .. 'z'];
var
  Word: String;
  QSel: TFDQuery;
  WordCount: Integer;
begin
  WordCount := 0;
  try
    // loop word in alphabet
    for Word in Alphabet do
    begin
      // create new query
      QSel := TFDQuery.Create(nil);
      // assign connection
      QSel.Connection := FDConnectionLite;
      // close connection
      QSel.Close;
      QSel.SQL.Text :=
        'select count(id) as total FROM dictionary WHERE word LIKE :word';
      QSel.Params.ParamByName('word').AsString := Word + '%' + Word;
      // active connection
      QSel.Active := true;
      // incress number
      Inc(WordCount, StrToInt(QSel.FieldValues['total']));
      // Free up resources
      QSel.Free;
    end;
    EditFirstLast.Text := IntToStr(WordCount);
    ProcessMemo.Lines.Add('Count char same first last complete. / '+TimeToStr(Now));
  except
    ProcessMemo.Lines.Add('Count char same first last fail. / '+TimeToStr(Now));
  end;
end;

procedure TForm1.CountCharSameMoreThanTwo;
const
  Alphabet = ['a' .. 'z'];
var
  Word: String;
  QSel: TFDQuery;
  WordCount: Integer;
begin
  WordCount := 0;
  try
    // loop word in alphabet
    for Word in Alphabet do
    begin
      // create new query
      QSel := TFDQuery.Create(nil);
      // assign connection
      QSel.Connection := FDConnectionLite;
      // close connection
      QSel.Close;
      QSel.SQL.Text :=
        'select count(id) as total FROM dictionary WHERE length(word) - length(replace(word, :word, '''')) >= 2';
      QSel.Params.ParamByName('word').AsString := Word;
      // active connection
      QSel.Active := true;
      // incress number
      Inc(WordCount, StrToInt(QSel.FieldValues['total']));
      // Free up resources
      QSel.Free;
    end;
    EditDuplicate.Text := IntToStr(WordCount);
    ProcessMemo.Lines.Add('Count char same more than two complete. / '+TimeToStr(Now));
  except
    ProcessMemo.Lines.Add('Count char same more than two fail. / '+TimeToStr(Now));
  end;
end;

procedure TForm1.UpdateFirstChar;
var
  QSel: TFDQuery;
begin
  try
    // create new query
    QSel := TFDQuery.Create(nil);
    // assign connection
    QSel.Connection := FDConnectionLite;
    // close connection
    QSel.Close;
    QSel.SQL.Text :=
      'UPDATE dictionary SET word = UPPER(SUBSTR(word,1,1))||LOWER(SUBSTR(word,2,LENGTH(word)))';
    // exec query
    QSel.ExecSQL;
    // Free up resources
    QSel.Free;
    ProcessMemo.Lines.Add('Update first char complete. / '+TimeToStr(Now));
  except
    ProcessMemo.Lines.Add('Update first char fail. / '+TimeToStr(Now));
  end;
end;

procedure TForm1.ZipDirectory;
var
  sr: TSearchRec;
  RowCount: Integer;
begin
  try
    // Try to find regular in the current dir
    if FindFirst(DefaultDirectory + '/*.*', faDirectory, sr) < 0 then
      exit
    else
      repeat
        // if directory and not . or ..
        if (sr.Attr = faDirectory) AND (sr.Name[1] <> '.') then
        begin
          // Zip directory
          TZipFile.ZipDirectoryContents(DefaultDirectory + '/' + sr.Name +
            '.zip', DefaultDirectory + '/' + sr.Name + '/',
            TZipCompression.zcDeflate);
        end;
      until FindNext(sr) <> 0;
    // Free up resources used by these successful finds
    FindClose(sr);
    ProcessMemo.Lines.Add('Zip directories complete. / '+TimeToStr(Now));
  Except
    begin
      ProcessMemo.Lines.Add('Zip directories fail. / '+TimeToStr(Now));
      exit
    end;
  end;
end;

procedure TForm1.CountMoreThanFive;
var
  QSel: TFDQuery;
begin
  try
    // create new query
    QSel := TFDQuery.Create(nil);
    // assign connection
    QSel.Connection := FDConnectionLite;
    // close connection
    QSel.Close;
    QSel.SQL.Text :=
      'select count(id) as total FROM dictionary WHERE LENGTH(word) > 5';
    // active connection
    QSel.Active := true;
    EditFiveChar.Text := QSel.FieldValues['total'];
    // Free up resources
    QSel.Free;
    ProcessMemo.Lines.Add('Count char more than five complete. / '+TimeToStr(Now));
  except
    ProcessMemo.Lines.Add('Count char more than five fail. / '+TimeToStr(Now));
  end;
end;

function TForm1.GetDirSize(dir: string; subdir: Boolean): Longint;
var
  sr: TSearchRec;
  found: Integer;
begin
  Result := 0;
  // check last path dir have \
  if dir[length(dir)] <> '\' then
    dir := dir + '\'; // assign \ to path dir
  found := FindFirst(dir + '*.*', faAnyFile, sr);
  while found = 0 do
  begin
    Inc(Result, sr.Size);
    if (sr.Attr and faDirectory > 0) and (sr.Name[1] <> '.') and (subdir = true)
    then
      Inc(Result, GetDirSize(dir + sr.Name, true));
    found := FindNext(sr);
  end;
  // Free up resources used by these successful finds
  FindClose(sr);
end;

procedure TForm1.ReportSizeDirectories;
var
  sr: TSearchRec;
  RowCount, found: Integer;
begin
  try
    // Try to find regular in the current dir
    found := FindFirst(DefaultDirectory + '/*.*', faDirectory, sr);
    while found = 0 do
    begin
      // if directory and not . or ..
      if (sr.Attr = faDirectory) AND (sr.Name[1] <> '.') then
      begin
        RowCount := StringGrid1.RowCount + 1;
        StringGrid1.RowCount := RowCount;
        StringGrid1.Cells[0, RowCount - 1] := sr.Name;
        StringGrid1.Cells[1, RowCount - 1] :=
          FloatToStr(GetDirSize(DefaultDirectory + '/' + sr.Name, true));
        GetSubInDirectories(DefaultDirectory + '/' + sr.Name);
      end;
      found := FindNext(sr);
    end;
    // Free up resources used by these successful finds
    FindClose(sr);
    ProcessMemo.Lines.Add('Get size directories complete. / '+TimeToStr(Now));
  Except
    begin
      ProcessMemo.Lines.Add('Get size directories fail. / '+TimeToStr(Now));
      exit
    end;
  end;
end;

procedure TForm1.GetSubInDirectories(dir: string);
var
  sr: TSearchRec;
  RowCount, found: Integer;
begin
  try
    // Try to find regular in the current dir
    found := FindFirst(dir + '/*.*', faAnyFile, sr);
    while found = 0 do
    begin
      // if directory and not . or ..
      if sr.Name[1] <> '.' then
      begin
        RowCount := StringGrid1.RowCount + 1;
        StringGrid1.RowCount := RowCount;
        StringGrid1.Cells[0, RowCount - 1] := '- ' + sr.Name;
        StringGrid1.Cells[1, RowCount - 1] := FloatToStr(sr.Size);
      end;
      found := FindNext(sr);
    end;
    // Free up resources used by these successful finds
    FindClose(sr);
  finally

  end;
end;

function TForm1.InsertData(Word: string): Boolean;
begin
  try
    Result := true;
    FDQInsert.ParamByName('word').AsString := Word;
    FDQInsert.ExecSQL();
  except
    on E: Exception do
      Result := FALSE;
  end;
end;

end.
